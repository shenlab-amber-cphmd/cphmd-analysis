#!/usr/bin/env python

import re
import matplotlib.pyplot as plt
import pandas as pd
import glob
import math

font = {'family': 'sans-serif',
        'color': 'black',
        'fontname': 'Liberation Sans',
        'weight': 'normal',
        'size': 14,
        }


def numericalSort(value):
    """
    A function serves as sorting key

    params:
        value: string, a string containes numbers

    returns:
        parts: list of strings and integers
    """
    numbers = re.compile(r'(\d+)')
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts


def plotLambda(lambda_file_wc="*.lambda", identifier='Prod', resids=[]):
    """
    A function to Plot lambda vs time figures for each resid at each pH condition.

    params:
        lambda_file_wc: string, file wild card for lambda files; default is "*.lambda"
        identifier: string, plot figure file names start with identifier; default is 'Prod'
        resids: list of integers, residue ids to plot the lambda values

    returns:
        None
    """
    # Check inputs
    lambda_files = sorted(glob.glob(lambda_file_wc), key=numericalSort)
    if not lambda_files:
        raise ValueError(
            "There is no file with wild card '{}'.".format(lambda_file_wc))
    x_label_name = 'Time/ns'
    y_label_name = 'lambda'
    if not resids:
        raise ValueError('Resids are required but not provided.')

    # Determine the subplot layout
    n_subplots = len(lambda_files)
    if n_subplots <= 7:
        col = n_subplots
    else:
        col = 6
    if n_subplots % 3 == 0:
        col = 3
    if n_subplots % 4 == 0:
        col = 4
    if n_subplots % 5 == 0:
        col = 5
    if n_subplots % 6 == 0:
        col = 6
    n_rows = math.ceil(n_subplots/col)
    n_cols = min(col, n_subplots)  # A row has a maximum of col cols
    fig_size = (3.0 * n_cols, 3.0 * n_rows)

    # Plot lambda data for each resid
    for resid in resids:
        fig, axs = plt.subplots(
            nrows=n_rows, ncols=n_cols, sharey=True, sharex=True, figsize=fig_size)
        axs = axs.flatten()
        for i, lambda_file in enumerate(lambda_files):
            # Assuming the pH value is in the file name and is the last number in the name.
            numbers = [s.strip('.')
                       for s in re.findall(r'-?\d+\.?\d*', lambda_file)]
            with open(lambda_file, 'r') as lf:
                _ = lf.readline()
                second_line = lf.readline()
                ires = second_line[7:].split()
            col_names = ['step']
            res_prev = '0'
            for res in ires:
                if res == res_prev:
                    col_names.append('{}_2'.format(res))
                else:
                    res_prev = res
                    col_names.append('{}_1'.format(res))
            data = pd.read_csv(
                lambda_file, comment='#', names=col_names, delim_whitespace=True, header=None)
            ns_per_frame = (data['step'][1] - data['step'][0]) * 0.002 * 0.001
            time = [ns_per_frame*(i+1) for i in range(len(data['step']))]
            data['time'] = time
            axs[i].scatter(data['time'], data['{}_1'.format(resid)],
                           marker='x', s=2.5, label='pH={}'.format(numbers[-1]))
            axs[i].set_ylim([0.0, 1.0])
            axs[i].legend(fontsize=font['size'], loc='center left')
            axs[i].tick_params(labelsize=font['size'])
            if i % n_cols == 0:
                axs[i].set_ylabel(y_label_name, fontdict=font)
            if n_subplots - i <= n_cols:
                axs[i].set_xlabel(x_label_name, fontdict=font)

        # Remove empty plots
        for i in range(n_rows * n_cols):
            if i >= n_subplots:
                axs[i].axis('off')

        # Export figures
        fig.tight_layout()
        out_fig = '{}_{}.png'.format(identifier, resid)
        fig.savefig(out_fig, bbox_inches='tight', transparent=False)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="Plot CpHMD lambda vs time figures for residues at multiple pH conditions.")
    parser.add_argument('-f', '--lambdafiles', default="*.lambda", dest='lambdafiles',
                        help="File wild card for lambda files to plot figure. [default: '*.lambda']")
    parser.add_argument('-i', '--identifier', default='Prod', dest='identifier',
                        help="A file identifier for lambda files. [default: Prod]'")
    parser.add_argument('-ids', '--resids', default=None, dest='resids',
                        help="Resids to plot, required. [default: None]")
    args = parser.parse_args()
    if args.resids is None:
        raise ValueError(
            'Resid(s) is(are) required by specifying the -ids|--resids flag. Use comma to separate multipe resids if necessary.')
    print('Warning: this script assumes the pH values are in the file names and are the last numbers in them.')
    plotLambda(lambda_file_wc=args.lambdafiles,
               identifier=args.identifier, resids=args.resids.split(','))
