#!/usr/bin/env python

import glob
import numpy as np
import re


def calFrac(fileWildCard='*prod?.lambda', identifier=None, minStep=0, maxStep=None, lowBound=0.2, highBound=0.8, splots=False, sintv=200):
    """

    This function is to calculate the unprot. frac for all residues at all pHs from the listed files by a wildcard.

    params:
        fileWildCard: String, a wildcard description of the lambda files to analyze, default is '*prod?.lambda'.
        identifier: String, a id used for labeling result data, default is None and the first 4 letters in the first file will be used as identifier.
        minStep: Integer, the start frame to calculate unprot. frac., default is 0.
        maxStep: Integer, the end frame to calculate unprot. frac., default is -1. 
        lowBound: Float, the lambda value below which is considered to be protonated, default is 0.2.
        highBound: Float, the lambda value above which is considered to be unprotonated, default is 0.8.

    returns:
        step[0:2]: list of integers, first two step time stamp for the CpHMD lambda files
        unprotfrac: dict, unprot fraction for each residue and pH condition.
    """
    files = glob.glob(fileWildCard)  # Expect xxxx*prod?.lambda
    files = sorted(files)
    if identifier is None:
        # Use the first 4 letter as identifier, expected as a PDB id.
        identifier = files[0][0:min(4, len(files[0]))]

    # Read in lambda files which has the same format as lambda file
    ititr = []  # Read from 1st line; ith titratable site
    ires = {}  # Read from 2nd line; the residue index of the ith titratable site
    itauto = []  # Read from 3rd line; tautomer type
    ParK = []  # Read from 4th line; Park values
    pH = []  # pH conditions, a list obtained from lambda file names
    unprotfrac = {}  # unprot frac for each pH conditions, for each residue; key is pH
    for i, lfile in enumerate(files):
        numbers = [s.strip('.') for s in re.findall(
            r'-?\d+\.?\d*', lfile[len(identifier):])]
        pH.append(numbers[0])
        # unprot frac for each residue; key is ititr
        unprotfrac[numbers[0]] = {}
        step = []
        lambda_values = []
        n_prots = []
        n_unprots = []
        n_mixs = []
        s_values = []
        # unprotfrac = []
        mix_p = []
        # n_unreals = []   # For evaluate the 'mixed' states between non-tautimers..
        n_exchange = []  # For evaluate the proton exchange rates between two states
        with open(lfile, 'r') as lf:
            for j in range(4):
                line = lf.readline()
                tokens = line.split()
                if i == 0 and len(tokens) > 0:  # Read info from the first lambda file
                    for k, token in enumerate(tokens[2:]):
                        if j == 0:
                            ititr.append(int(token))
                        elif j == 1:
                            # ires.append(token)
                            ires[ititr[k]] = token
                        elif j == 2:
                            itauto.append(token)
                        else:
                            ParK.append(token)
            for line in lf:
                tokens = line.split()
                if len(tokens) > 0 and line[0] != '#':
                    step.append(tokens[0])
                    lambda_values.append([])
                    for token in tokens[1:]:
                        lambda_values[-1].append(float(token))
        lambda_values = np.array(lambda_values)
        for it in ititr:  # loop over each col
            # unprotfrac[numbers[0]][it] = [] # list of S as a function of time
            n_prot = 0
            n_unprot = 0
            n_mix = 0
            n_unreal = 0
            num_exchange = -1
            for ii, value in enumerate(lambda_values[minStep:maxStep, it-1]):
                if itauto[it-1] == '0':  # Cys/Lys/Tyr
                    if value < lowBound:
                        n_prot += 1
                    elif value > highBound:
                        n_unprot += 1
                    else:
                        n_mix += 1
                elif itauto[it-1] == '1':  # His macro
                    #                if value < lowBound:
                    if value < lowBound and (lambda_values[ii+minStep, it] > highBound or lambda_values[ii+minStep, it] < lowBound):
                        n_prot += 1
                    elif value > highBound and (lambda_values[ii+minStep, it] > highBound or lambda_values[ii+minStep, it] < lowBound):
                        n_unprot += 1
                    else:
                        n_mix += 1
                elif itauto[it-1] == '2':  # His micro
                    if lambda_values[ii, it-2] > highBound and value > highBound:
                        n_unprot += 1
                    elif lambda_values[ii, it-2] > highBound and value < lowBound:
                        n_prot += 1
                    elif lambda_values[ii, it-2] > highBound and lowBound <= value and value <= highBound:
                        n_mix += 1
                    elif lambda_values[ii, it-2] < lowBound and lowBound <= value and value <= highBound:
                        n_unreal += 1
                elif itauto[it-1] == '3':
                    #                if value > highBound:
                    if value > highBound and (lambda_values[ii+minStep, it] > highBound or lambda_values[ii+minStep, it] < lowBound):
                        n_unprot += 1
                    elif value < lowBound and (lambda_values[ii+minStep, it] > highBound or lambda_values[ii+minStep, it] < lowBound):
                        n_prot += 1
                    else:
                        n_mix += 1
                elif itauto[it-1] == '4':
                    if lambda_values[ii, it-2] < lowBound and value > highBound:
                        n_unprot += 1
                    elif lambda_values[ii, it-2] < lowBound and value < lowBound:
                        n_prot += 1
                    elif lambda_values[ii, it-2] < lowBound and lowBound <= value and value <= highBound:
                        n_mix += 1
                    elif lambda_values[ii, it-2] > highBound and lowBound <= value and value <= highBound:
                        n_unreal += 1
                # Calculate proton exchange number
                # print(ires[it])
                if ii == 0 and (itauto[it-1] == '0' or itauto[it-1] == '1' or itauto[it-1] == '3'):
                    unprotfrac[numbers[0]][ires[it]] = []
                if splots and (itauto[it-1] == '0' or itauto[it-1] == '1' or itauto[it-1] == '3') and ((ii+1) % sintv == 0):
                    if n_prot + n_unprot == 0:
                        print('Temperarily no protonated or deprotonated state!')
                        p_unprot_tmp = 0
                    else:
                        p_unprot_tmp = n_unprot / (n_prot + n_unprot)
                    unprotfrac[numbers[0]][ires[it]].append(p_unprot_tmp)
                if (value > highBound or value < lowBound) and num_exchange == -1:
                    num_exchange = 0
                    tmp_value = value
                elif num_exchange >= 0 and abs(value - tmp_value) > (highBound - lowBound) and (value > highBound or value < lowBound):
                    num_exchange += 1
                    tmp_value = value
            n_exchange.append(num_exchange)
            n_prots.append(n_prot)
            n_unprots.append(n_unprot)
            n_mixs.append(n_mix)
 #           n_unreals.append(n_unreal / ii)
            if itauto[it-1] == '0' or itauto[it-1] == '1' or itauto[it-1] == '3':
                if n_prot + n_unprot == 0:
                    print('No protonated or deprotonated state!')
                    exit()
                else:
                    p_unprot = n_unprot / (n_prot + n_unprot)
            elif itauto[it-1] == '2':
                if n_prot + n_prots[-2] == 0:
                    p_taut1 = 1.0
                else:
                    p_taut1 = n_prot / (n_prot + n_prots[-2])
                    # p_taut1 = n_prot / (n_prot + n_unprot) # For Sx
                if n_unprot + n_prots[-2] == 0:
                    p_taut2 = 1.0
                else:
                    # p_taut2 = n_unprot / (n_prot + n_unprot)  # For Sx
                    p_taut2 = n_unprot / (n_unprot + n_prots[-2])
            elif itauto[it-1] == '4':
                if n_prot + n_unprots[-2] == 0 or n_prot + n_unprot == 0:
                    p_taut1 = 1.0
                else:
                    # p_taut1 = n_prot / (n_prot + n_unprot) # For Sx
                    p_taut1 = n_unprots[-2] / (n_prot + n_unprots[-2])
                if n_unprot + n_unprots[-2] == 0 or n_prot + n_unprot == 0:
                    p_taut2 = 1.0
                else:
                    # p_taut2 = n_unprot / (n_prot + n_unprot)  # For Sx
                    p_taut2 = n_unprots[-2] / (n_unprot + n_unprots[-2])
            p_mix = n_mix / ii
            # Note that for HIS, ASP and GLU
            # this is not the mix state rates between two states in each coordinate but the total
            # percentage of unphysical states
            if itauto[it-1] == '0' or itauto[it-1] == '1' or itauto[it-1] == '3':
                s_values.append('{:.3f}'.format(p_unprot))
                # unprotfrac.append(p_unprot)
                if len(numbers) == 1:
                    savedata = '{}-{}.data'.format(identifier, ires[it])
                else:
                    savedata = '{}-{}-{}.data'.format(
                        identifier, ires[it], numbers[-1])
            elif itauto[it-1] == '2' or itauto[it-1] == '4':
                s_values.append('{:.3f} {:.3f}'.format(p_taut1, p_taut2))
                if len(numbers) == 1:
                    # for micro pKa
                    savedata = '{}-{}.micro_data'.format(identifier, ires[it])
                else:
                    savedata = '{}-{}-{}.micro_data'.format(
                        identifier, ires[it], numbers[-1])
            mix_p.append('{:.3f}'.format(p_mix))
            if i == 0:
                with open(savedata, 'w+') as sf:
                    sf.write('# pH S mixed num_exchange\n')
                    sf.write('{} {} {} {}\n'.format(
                        pH[-1], s_values[-1], mix_p[-1], n_exchange[-1]))
            else:
                with open(savedata, 'a+') as sf:
                    sf.write('{} {} {} {}\n'.format(
                        pH[-1], s_values[-1], mix_p[-1], n_exchange[-1]))
    return step[0:2], unprotfrac


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="A Python program to calculate the unprotonation fraction from CpHMD calculation results.")
    parser.add_argument('-f', '--files', default='*prod1.lambda', dest='files', metavar='FILES',
                        help="Wildcard for lambda files generated by CpHMD simulations [default: *prod1.lambda]")
    parser.add_argument('-id', '--identifier', default=None, dest='id', metavar='ID',
                        help='An identifier (e.g. PDB ID) to label analysis result files [default: None]')
    parser.add_argument('-min', '--minstep', default=0, dest='minstep', metavar='MINSTEP',
                        help="The start frame to calculate unprotonation fraction. [default: 0]")
    parser.add_argument('-max', '--maxstep', default=None, dest='maxstep', metavar='MAXSTEP',
                        help="The end frame to calculate unprotonation fraction. [default: -1]")
    parser.add_argument('-lb', '--lowbound', default=0.2, dest='lowbound', metavar='LOWBOUND',
                        help="The fraction value below which is considered as being protonated. [default: 0.2]")
    parser.add_argument('-hb', '--highbound', default=0.8, dest='highbound', metavar='HIGHBOUND',
                        help="The fraction value above which is considered as being unprotonated. [default: 0.8]")
    parser.add_argument('-splots', '--svstimeplots', default=False, dest='splots',
                        metavar='SPLOTS', help="Whether to plot S vs time convergence. [default: True]")
    parser.add_argument('-sintv', '--splotsinterval', default=50, dest='sintv',
                        metavar='SINTV', help="Time interval to calculate accumalated S. [default: 50]")
    parser.add_argument('-sresids', '--splotsresids', default=None, dest='sresids',
                        metavar='SRESIDS', help="CpHMD residue ids to plot S vs Time data. [default: None]")
    args = parser.parse_args()
    if args.maxstep is None:
        max_step = None
    else:
        max_step = int(args.maxstep)
    first_two, pH_frac = calFrac(fileWildCard=args.files, splots=args.splots, sintv=int(args.sintv), identifier=args.id, minStep=int(
        args.minstep), maxStep=max_step, lowBound=float(args.lowbound), highBound=float(args.highbound))
    if args.splots:
        ns_per_frame = (float(first_two[1]) -
                        float(first_two[0])) * 0.002 * 0.001
        sintv = int(args.sintv)
        sresids = args.sresids.split(',')
        current = sintv
        if sresids:
            for sresid in sresids:
                splot_data = 'Res{}_SvsTime.data'.format(sresid)
                with open(splot_data, 'w') as sf:
                    sf.write('# Time(ns)')
                    max_len = 0
                    for pH in pH_frac:
                        if len(pH_frac[pH][sresid]) > max_len:
                            max_len = len(pH_frac[pH][sresid])
                        sf.write(' {}'.format(pH))
                    sf.write('\n')
                    for i in range(max_len):
                        sf.write('{:.3f}'.format((i+1)*sintv*ns_per_frame))
                        for ph in pH_frac:
                            try:
                                sf.write(' {:.3f}'.format(
                                    pH_frac[ph][sresid][i]))
                            except IndexError:
                                sf.write(' None')
                        sf.write('\n')
