The Python cphmdanalysis.py parses and runs calculations on the lambda and log files produced by
continuous constant pH molecular dynamics (CpHMD) simulations at independent pH conditions
or with pH replica exchange protocol.

For the latter case, two different classes are used to process the log files
class:log_analysis_charmm and class:log_analysis_amber.

All lambda files are processed with class:lambda_data.

Plots can be made using class:plotting_lambda_data for lambda files and class for
log files.


